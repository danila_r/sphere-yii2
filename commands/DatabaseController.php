<?php

namespace app\commands;

use app\helpers\AppHelper;
use app\models\Employee;
use app\models\Position;
use app\models\Skill;
use Exception;
use yii\console\Controller;
use yii\console\ExitCode;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

class DatabaseController extends Controller
{
    /**
     * Создает все начальные данные
     */
    public function actionInit()
    {
        $this->run('init-positions');
        $this->run('init-skills');
        $this->run('init-employees');
    }

    /**
     * Создает начальный набор сотрудников
     *
     * @return int
     * @throws Exception
     */
    public function actionInitEmployees(): int
    {
        $employees = [
            [
                'id' => 1,
                'first_name' => 'Даниил',
                'middle_name' => 'Борисович',
                'last_name' => 'Халецкий',
                'email' => 'danila@test.com',
                'phone' => '375291234567',
                'position_id' => 1,
                'skill_ids' => [1, 2, 3, 4, 5],
                'password_hash' => AppHelper::generatePasswordHash('1234'),
                'token' => '9llqhV0yCkNWbb2v5Sd6mE6amcbKoj4PzRP5uhhVRrMZO547YA1-YPGxC3k33-Zm',
                'role' => Employee::ROLE_ADMIN,
            ],
            [
                'id' => 2,
                'first_name' => 'Мария',
                'middle_name' => 'Алексеевна',
                'last_name' => 'Грушковская',
                'email' => 'maria@test.com',
                'phone' => '375291234568',
                'position_id' => 2,
                'skill_ids' => [6, 7],
                'password_hash' => AppHelper::generatePasswordHash('1234'),
                'token' => '0bz5mN6lM8v_-qqW39kuE_aPp2iTHVuDN74A37eyXWFWyW5Xh9ZfPCiq-PlIqTH6',
                'role' => Employee::ROLE_USER,
            ],
        ];

        $this->createEntities(Employee::class, $employees);
        return ExitCode::OK;
    }

    /**
     * Создает начальный набор должностей
     *
     * @return int
     * @throws Exception
     */
    public function actionInitPositions(): int
    {
        $positions = [
            ['id' => 1, 'name' => 'Директор'],
            ['id' => 2, 'name' => 'Главный бухгалтер'],
            ['id' => 3, 'name' => 'Бухгалтер'],
            ['id' => 4, 'name' => 'Экономист'],
            ['id' => 5, 'name' => 'Менеджер'],
        ];

        $this->createEntities(Position::class, $positions);
        return ExitCode::OK;
    }

    /**
     * Создает начальный набор навыков
     *
     * @return int
     * @throws Exception
     */
    public function actionInitSkills(): int
    {
        $positions = [
            ['id' => 1, 'name' => 'умение находить подход к людям'],
            ['id' => 2, 'name' => 'умение планировать'],
            ['id' => 3, 'name' => 'умение самостоятельно принимать решения'],
            ['id' => 4, 'name' => 'умение делегировать задачи'],
            ['id' => 5, 'name' => 'организаторские способности'],
            ['id' => 6, 'name' => 'умение расставлять приоритеты'],
            ['id' => 7, 'name' => 'внимательность к мелочам'],
            ['id' => 8, 'name' => 'умение улаживать конфликты'],
            ['id' => 9, 'name' => 'работа в группе'],
        ];

        $this->createEntities(Skill::class, $positions);
        return ExitCode::OK;
    }

    /**
     * Напечатать всех пользователей
     *
     * @return int
     */
    public function actionPrintUsers(): int
    {
        $users = Employee::find()->all();
        foreach ($users as $user) {
            foreach ($user->toArray() as $key => $value) {
                echo "$key: $value" . PHP_EOL;
            }
            echo PHP_EOL;
        }
        return ExitCode::OK;
    }

    /**
     * Создаеи в базе набор сущностей
     *
     * @param string $entityClass класс сущности
     * @param array $data массив с данными для заполнения полей
     *
     * @throws Exception
     */
    protected function createEntities(string $entityClass, array $data)
    {
        if (!is_subclass_of($entityClass, ActiveRecord::class)) {
            throw new Exception('entityClass must be subclass of ActiveRecord');
        }

        $db = $entityClass::getDb();
        $tableName = $entityClass::tableName();
        $sequenceName = ArrayHelper::getValue($db->getTableSchema($tableName), 'sequenceName');

        $transaction = $db->beginTransaction();
        try {
            $db->createCommand("truncate $tableName CASCADE")->execute();
            foreach ($data as $datum) {
                /** @var ActiveRecord $entity */
                $entity = new $entityClass($datum);
                if (!$entity->save()) {
                    throw new Exception(
                        sprintf(
                            "Entity don't save.\nClass: %s \nData: %s\nErrors: %s",
                            $entityClass,
                            print_r($datum, true),
                            print_r($entity->errors, true)
                        )
                    );
                }
            }

            // Если у первичного ключа есть последовательность, выставляем на максимальное значение
            if (!empty($sequenceName)) {
                $sql = sprintf(
                    "SELECT setval('%s', (SELECT MAX(id) FROM %s AS max_value))",
                    $sequenceName,
                    $tableName
                );
                $db->createCommand($sql)->execute();
            }
            $transaction->commit();
        } catch (Exception $exception) {
            $transaction->rollBack();
            throw $exception;
        }
    }
}
