<?php

return [
    // Служебные
    'status' => 'status/index',
    'status/exception' => 'status/exception',
    'status/error' => 'status/error',

    // Сотрудники
    ['class' => 'yii\rest\UrlRule', 'controller' => 'employee', 'pluralize' => false],

    // Должности
    ['class' => 'yii\rest\UrlRule', 'controller' => 'position', 'pluralize' => false],

    // Навыки
    ['class' => 'yii\rest\UrlRule', 'controller' => 'skill', 'pluralize' => false],
];