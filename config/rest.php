<?php

return [
    'id' => 'sphere-yii2',
    'basePath' => dirname(__DIR__),
    'defaultRoute' => 'status/index',
    'aliases' => [
        '@webroot' => '@app/public',
    ],
    'components' => [
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => true,
            'rules' => require ('routes.php'),
        ],
        'formatter' => [
            'datetimeFormat' => 'php:Y-m-d H:i:s',
        ],
        'request' => [
            'parsers' => [
                'application/json' => 'yii\web\JsonParser'
            ],
            'enableCookieValidation' => false,
            'enableCsrfValidation' => false,
            'enableCsrfCookie' => false,
        ],
        'response' => [
            'class' => app\common\RestResponse::class,
        ],
        'db' => require ('db.php'),
        'user' => [
            'identityClass' => 'app\models\Employee',
            'enableSession' => false,
            'enableAutoLogin' => false,
        ],
    ],
];
