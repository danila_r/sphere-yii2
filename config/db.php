<?php
$dns = sprintf('pgsql:host=%s;port=%s;dbname=%s',
    env('DB_HOST', 'localhost'),
    env('DB_PORT', 5432),
    env('DB_NAME', 'postgres')
);

return [
    'class' => 'yii\db\Connection',
    'dsn' => $dns,
    'username' => env('DB_USER', 'postgres'),
    'password' => env('DB_PASSWORD', ''),
    'charset' => 'utf8',
];