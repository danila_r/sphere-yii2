<?php

return [
    'id' => 'sphere-yii2-console',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'app\commands',
    'components' => [
        'db' => require ('db.php'),
    ],
];
