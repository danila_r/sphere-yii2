<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%positions}}`.
 */
class m220528_135715_create_positions_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%positions}}', [
            'id' => $this->bigPrimaryKey()->unsigned(),
            'name' => $this->string(64)->unique()->notNull(),
        ]);
        $this->addForeignKey(
            '{{%fk_employees_positions}}',
            '{{%employees}}',
            'position_id',
            '{{%positions}}',
            'id',
            'SET NULL',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('{{%fk_employees_positions}}', '{{%employees}}');
        $this->dropTable('{{%positions}}');
    }
}
