<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%employee_skills}}`.
 */
class m220530_165858_create_employee_skills_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%employee_skills}}', [
            'employee_id' => $this->bigInteger()->unsigned()->notNull(),
            'skill_id' => $this->bigInteger()->unsigned()->notNull(),
        ]);
        $this->addPrimaryKey('{{%pk_employee_skills}}', '{{%employee_skills}}', ['employee_id', 'skill_id']);
        $this->addForeignKey(
            '{{%fk_employee_skills_employees}}',
            '{{%employee_skills}}',
            'employee_id',
            '{{%employees}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            '{{%fk_employee_skills_skills}}',
            '{{%employee_skills}}',
            'skill_id',
            '{{%skills}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->createIndex('{{%idx_employee_skills_skill_id}}', '{{%employee_skills}}', ['skill_id']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%employee_skills}}');
    }
}
