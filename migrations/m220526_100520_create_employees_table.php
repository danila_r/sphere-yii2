<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%employees}}`.
 */
class m220526_100520_create_employees_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%employees}}', [
            'id' => $this->bigPrimaryKey()->unsigned(),
            'first_name' => $this->string(64)->notNull(),
            'middle_name' => $this->string(64)->notNull(),
            'last_name' => $this->string(64)->notNull(),
            'email' => $this->string(254)->unique()->notNull(), //RFC 2821
            'phone' => $this->string(15)->notNull(), //E.164
            'position_id' => $this->bigInteger()->unsigned()->null(),
            'password_hash' => $this->string(255)->notNull(),
            'role' => $this->smallInteger()->notNull(),
            'token' => $this->string(64)->unique()->null(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%employees}}');
    }
}
