<?php

namespace app\models;

use yii\db\ActiveRecord;

class EmployeeSkills extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return '{{%employee_skills}}';
    }
}