<?php
namespace app\models;

use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * @property int $id
 * @property string $name
 */
class Position extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public function fields(): array
    {
        return [
            'id',
            'name',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['name'], 'required'],
            [['name'], 'unique'],
        ];
    }

    /**
     * Связь с сотрудниками на этой должности
     *
     * @return ActiveQuery
     */
    public function getEmployees(): ActiveQuery
    {
        return $this->hasMany(Employee::class, ['position_id' => 'id']);
    }

    /**
     * Кол-во сотрудников с данной должностью
     *
     * @return int
     */
    public function getEmployeeCount(): int
    {
        return $this->getEmployees()->count();
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return '{{%positions}}';
    }
}
