<?php

namespace app\models;

use app\helpers\AppHelper;
use app\validators\ArrayMaxLengthValidator;
use app\validators\RoleValidator;
use DateTime;
use yii\base\Exception;
use yii\base\InvalidConfigException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * @property int $id
 * @property string $first_name
 * @property string $middle_name
 * @property string $last_name
 * @property string $email
 * @property string $phone
 * @property int|null $position_id
 * @property string $password_hash
 * @property int $role
 * @property string $token
 * @property string $created_at
 * @property string $updated_at
 *
 * @property string $fullName
 * @property string $isAdmin
 */
class Employee extends ActiveRecord implements IdentityInterface
{
    const ROLE_GUEST = 0;
    const ROLE_USER = 1;
    const ROLE_ADMIN = 2;

    const SCENARIO_CREATE = 'create';
    const SCENARIO_UPDATE = 'update';

    public ?array $skill_ids = null;

    /** @var string|null Атрибут для передачи пароля при заполнении из запроса */
    public ?string $password = null;

    /**
     * {@inheritdoc}
     */
    public function fields(): array
    {
        return [
            'id',
            'full_name' => 'fullName',
            'email',
            'phone',
            'position',
            'skills',
            'role',
            'created_at',
            'updated_at',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['first_name', 'middle_name', 'last_name'], 'string', 'max' => 64,],
            [['phone'], 'string', 'max' => 15],
            [['email'], 'email'],
            [['email'], 'unique'],
            [['password'], 'string', 'max' => 16],
            [
                ['first_name', 'middle_name', 'last_name', 'phone', 'email', 'password'],
                'required',
                'on' => [self::SCENARIO_CREATE]
            ],

            [['role'], 'in', 'range' => [self::ROLE_USER, self::ROLE_ADMIN], 'on' => [self::SCENARIO_DEFAULT]],
            [['role'], RoleValidator::class, 'on' => [self::SCENARIO_CREATE, self::SCENARIO_UPDATE]],
            [['role'], 'default', 'value' => self::ROLE_USER, 'on' => [self::SCENARIO_CREATE]],

            [['position_id'], 'integer'],
            [['position_id'], 'exist', 'targetClass' => Position::class, 'targetAttribute' => 'id'],

            [['skill_ids'], 'each', 'rule' => ['integer']],
            [['skill_ids'], ArrayMaxLengthValidator::class, 'max' => 5],
            [['skill_ids'], 'exist', 'targetClass' => Skill::class, 'targetAttribute' => 'id', 'allowArray' => true],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors(): array
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'value' => AppHelper::dateTimeFormat(new DateTime()),
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function afterFind()
    {
        // Postgres драйвер возвращает timestamp как строку в своем формате,
        // поэтому преобразовываем в стандарт приложения
        $this->created_at = AppHelper::dateTimeFormat($this->created_at);
        $this->updated_at = AppHelper::dateTimeFormat($this->updated_at);
        parent::afterFind();
    }

    /**
     * {@inheritdoc}
     *
     * @throws Exception
     */
    public function beforeSave($insert): bool
    {
        // Если в модель передан новый пароль, присваиваем его хеш
        if (!is_null($this->password)) {
            $this->setPassword($this->password);
        }
        return parent::beforeSave($insert);
    }

    /**
     * {@inheritdoc}
     *
     * @throws Exception
     */
    public function afterSave($insert, $changedAttributes)
    {
        // Если в модель передан новый массив навыков, обновляем связи
        if (!is_null($this->skill_ids)) {
            $this->updateSkills($this->skill_ids);
        }
        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * Обновляет связи навыков сотрудника
     *
     * @param array $skillIds Масссив ID навыков
     *
     * @throws Exception
     */
    public function updateSkills(array $skillIds)
    {
        $db = EmployeeSkills::getDb();
        $transaction = $db->beginTransaction();
        try {
            EmployeeSkills::deleteAll(['employee_id' => $this->id]);
            $db
                ->createCommand()
                ->batchInsert(
                    EmployeeSkills::tableName(),
                    ['employee_id', 'skill_id'],
                    array_map(fn($id) => [$this->id, $id], $skillIds))
                ->execute();
            $transaction->commit();
        } catch (Exception $exception) {
            $transaction->rollBack();
            throw $exception;
        }
    }

    /**
     * Формирует полное имя сотрудника
     *
     * @return string
     */
    public function getFullName(): string
    {
        return implode(' ', array_filter([
            $this->last_name,
            $this->first_name,
            $this->first_name ? $this->middle_name : null
        ]));
    }

    /**
     * Проверяет сотрудника на принадлежность к группе администраторов
     *
     * @return bool
     */
    public function getIsAdmin(): bool
    {
        return $this->role === self::ROLE_ADMIN;
    }

    /**
     * @param string $password
     *
     * @throws Exception
     */
    public function setPassword(string $password)
    {
        $this->password_hash = AppHelper::generatePasswordHash($password);
    }

    /**
     * Проверка пароля
     *
     * @param string $password
     *
     * @return bool
     */
    public function validatePassword(string $password): bool
    {
        return AppHelper::validatePasswordHash($password, $this->password_hash);
    }

    /**
     * Связь с должностью сотрудника
     *
     * @return ActiveQuery
     */
    public function getPosition(): ActiveQuery
    {
        return $this->hasOne(Position::class, ['id' => 'position_id']);
    }

    /**
     * Связь с навыками сотрудника
     *
     * @throws InvalidConfigException
     */
    public function getSkills(): ActiveQuery
    {
        return $this
            ->hasMany(Skill::class, ['id' => 'skill_id'])
            ->viaTable('{{%employee_skills}}', ['employee_id' => 'id']);
    }

    /**
     * Поиск пользователя по ID
     *
     * @param int|string $id
     *
     * @return Employee|IdentityInterface|null
     */
    public static function findIdentity($id): self
    {
        return static::findOne($id);
    }

    /**
     * Поиск пользователя по токену
     *
     * @param mixed $token
     * @param null $type
     *
     * @return null
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['token' => $token]);
    }

    /**
     * Возвращает идентификатор пользователя
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Не поддерживается
     *
     * @return null
     */
    public function getAuthKey()
    {
        return null;
    }

    /**
     * Не поддерживается
     *
     * @param string $authKey
     *
     * @return null
     */
    public function validateAuthKey($authKey)
    {
        return null;
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return '{{%employees}}';
    }
}