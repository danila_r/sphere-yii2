<?php
namespace app\validators;

use yii\validators\Validator;

class ArrayMaxLengthValidator extends Validator
{
    /** @var int|null максимальная длинна массива */
    public ?int $max = null;

    /**
     * {@inheritdoc}
     */
    public function validateValue($value): ?array
    {
        if ($this->max && is_array($value) && count($value) > $this->max) {
            return [
                '{attribute} should contain at most {max, number} ids.',
                ['max' => $this->max]
            ];
        }

        return null;
    }
}