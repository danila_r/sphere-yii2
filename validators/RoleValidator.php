<?php
namespace app\validators;

use app\helpers\AppHelper;
use app\models\Employee;
use yii\base\InvalidConfigException;
use yii\validators\RangeValidator;

class RoleValidator extends RangeValidator
{
    /** @var array */
    public $range = [Employee::ROLE_USER, Employee::ROLE_ADMIN];

    /**
     * {@inheritdoc}
     * @throws InvalidConfigException
     */
    public function validateAttribute($model, $attribute)
    {
        parent::validateAttribute($model, $attribute);
        if (!$model->hasErrors($attribute)) {
            if ($model->$attribute == Employee::ROLE_ADMIN && !AppHelper::currentEmployee()->isAdmin) {
                $this->addError($model, $attribute, 'Only Admins can set the Admin role');
            }
        }
    }
}
