<?php
namespace app\common;

use yii\web\Response;

class RestResponse extends Response
{
    public $format = self::FORMAT_JSON;

    /**
     * {@inheritdoc}
     */
    public function send(): void
    {
        if (!empty($this->data)) {
            $this->data = [
                'success' => $this->isSuccessful,
                'data' => $this->data,
            ];
        }
        parent::send();
    }
}