<?php
namespace app\common;

use yii\rest\Serializer;

class RestSerializer extends Serializer
{
    /** @var array|null строгий набор полей модели */
    public ?array $fields = null;

    /** @var array|null строгий расширенный набор полей модели */
    public ?array $expandFields = null;

    public $collectionEnvelope = 'items';
    public $linksEnvelope = 'links';
    public $metaEnvelope = 'meta';

    /**
     * {@inheritdoc}
     */
    protected function getRequestedFields(): array
    {
        list($fields, $expand) = parent::getRequestedFields();
        return [
            is_null($this->fields) ? $fields : $this->fields,
            is_null($this->expandFields) ? $expand : $this->expandFields,
        ];
    }
}
