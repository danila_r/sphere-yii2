<?php

namespace app\helpers;

use app\models\Employee;
use yii;
use yii\base\Exception;
use yii\base\InvalidConfigException;

class AppHelper
{
    /**
     * Возвращает инстанс приложения
     *
     * @return yii\base\Application|yii\console\Application|yii\web\Application
     */
    public static function app()
    {
        return Yii::$app;
    }

    /**
     * Генерирует хэш пароля
     *
     * @param string $password
     *
     * @return string
     *
     * @throws Exception
     */
    public static function generatePasswordHash(string $password): string
    {
        return self::app()->security->generatePasswordHash($password);
    }

    /**
     * Проверяет хэш пароля
     *
     * @param string $password
     * @param string $hash
     *
     * @return bool
     */
    public static function validatePasswordHash(string $password,  string $hash): bool
    {
        return self::app()->security->validatePassword($password, $hash);
    }

    /**
     * Генерирует токен доступа
     *
     * @return string
     *
     * @throws Exception
     */
    public static function generateAccessToken(): string
    {
        return self::app()->security->generateRandomString(64);
    }

    /**
     * Врозвращает сущность авторизованного сотрудника или null-объект
     *
     * @return Employee
     * @throws InvalidConfigException
     */
    public static function currentEmployee(): Employee
    {
        $app = self::app();
        $user = $app instanceof yii\web\Application ? $app->user->identity : null;
        if (!($user instanceof Employee)) {
            $user = Yii::createObject(Employee::class, [
                'id' => 0,
                'first_name' => 'Гость',
                'role' => Employee::ROLE_GUEST,
            ]);
        }
        return $user;
    }

    /**
     * Получить значение параметра приложения
     *
     * @param string $name имя параметра
     * @param mixed $default значение по умолчанию если параметр не установлен
     *
     * @return mixed|null
     */
    public static function getParam(string $name, $default = null)
    {
        try {
            return yii\helpers\ArrayHelper::getValue(self::app()->params, $name, $default);
        } catch (\Exception $exception) {
            return $default;
        }
    }

    /**
     * @param mixed $value дат/время в лбой доступной форме
     *
     * @return string
     */
    public static function dateTimeFormat($value): string
    {
        try {
            return self::app()->formatter->asDatetime($value);
        } catch (InvalidConfigException $e) {
            return '';
        }
    }
}