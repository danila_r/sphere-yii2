<?php
namespace app\controllers;

use app\common\RestSerializer;
use app\helpers\AppHelper;
use app\models\Employee;
use yii\base\InvalidConfigException;
use yii\filters\auth\HttpBearerAuth;
use yii\rest\ActiveController;
use yii\web\ForbiddenHttpException;

class EmployeeController extends ActiveController
{
    public $modelClass = Employee::class;
    public $serializer = ['class' => RestSerializer::class];
    public $createScenario = Employee::SCENARIO_CREATE;
    public $updateScenario = Employee::SCENARIO_UPDATE;

    /**
     * {@inheritdoc}
     */
    public function behaviors(): array
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::class,
            'optional' => ['index', 'view', 'create'],
        ];
        return $behaviors;
    }

    /**
     * Проверка доступа:
     * - Гость: только просмотр
     * - Пользователь: просмотр и изменение
     * - Админ: все действия
     *
     * @param string $action
     * @param Employee $model
     * @param array $params
     *
     * @throws ForbiddenHttpException
     * @throws InvalidConfigException
     */
    public function checkAccess($action, $model = null, $params = [])
    {
        $currentUser = AppHelper::currentEmployee();

        // Админам можно все
        if ($currentUser->isAdmin) {
            return;
        }

        // Запрет для остальных пользователей удалять сотрудников и измененять данные других сотрудников
        if ($action === 'delete' || ($action === 'update' && $model->id !== $currentUser->id)) {
            throw new ForbiddenHttpException();
        }

        // Ограничение видимости полей для незарегистрированных пользователей
        if ($currentUser->role == Employee::ROLE_GUEST) {
            $this->serializer['fields'] = [
                'id',
                'position',
                'skills',
            ];
        }
    }
}