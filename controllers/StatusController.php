<?php
namespace app\controllers;

use yii\rest\Controller;
use DateTime;
use DateTimeInterface;
use Exception;
use Yii;
use yii\db\Exception as dbException;

class StatusController extends Controller
{
    /**
     * Статус приложения, для проверки сростояния и тестов
     *
     * @return string[]
     */
    public function actionIndex(): array
    {
        return [
            'http' => 'success',
            'database' => $this->checkDbConnection() ? 'success' : 'failure',
            'now' => (new DateTime())->format(DateTimeInterface::RFC3339),
        ];
    }

    /**
     * Повидение при не обработанном исключении, для проверки и тестов
     *
     * @throws Exception
     */
    public function actionException()
    {
        throw new Exception("Test application exception");
    }

    /**
     * Повидение при не фатальной PHP ошибке, для проверки и тестов
     */
    public function actionError()
    {
        $x = 5/0;
    }

    /**
     * Проверка соединения с БД
     *
     * @return bool
     */
    private function checkDbConnection(): bool
    {
        try {
            return Yii::$app->db->createCommand('SELECT 1')->queryScalar() === 1;
        } catch (dbException $exception) {
            return false;
        }
    }
}