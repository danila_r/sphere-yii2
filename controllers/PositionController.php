<?php
namespace app\controllers;

use app\common\RestSerializer;
use app\helpers\AppHelper;
use app\models\Position;
use yii\base\InvalidConfigException;
use yii\filters\auth\HttpBearerAuth;
use yii\rest\ActiveController;
use yii\web\ForbiddenHttpException;

class PositionController extends ActiveController
{
    public $modelClass = Position::class;
    public $serializer = ['class' => RestSerializer::class];

    /**
     * {@inheritdoc}
     */
    public function behaviors(): array
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::class,
            'optional' => ['index', 'view'],
        ];
        return $behaviors;
    }

    /**
     * {@inheritdoc}
     */
    public function actions(): array
    {
        // Выключаем пагинацию для спичка должностей
        $actions = parent::actions();
        if (isset($actions['index'])) {
            $actions['index']['pagination'] = false;
        }
        return $actions;
    }

    /**
     * Проверка доступа для создания, изменения и удаления должности
     *
     * @throws ForbiddenHttpException
     * @throws InvalidConfigException
     */
    public function checkAccess($action, $model = null, $params = [])
    {
        $currentUser = AppHelper::currentEmployee();

        // Удалять и изменять должности можно только Админам
        if (!$currentUser->isAdmin && in_array($action, ['create', 'delete', 'update'])) {
            throw new ForbiddenHttpException('For Admins only access');
        }
    }
}